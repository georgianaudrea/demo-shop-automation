package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.Selenide.$;
import static org.testng.Assert.assertEquals;


public class CartTest extends DemoShopConfiguration {
    private SelenideElement cartButton = $("[href='#/cart']");
    private SelenideElement shoppingCartBadge = $(".shopping_cart_badge");

    private SelenideElement continueShopping = $("[href='#/products']");

    private SelenideElement homeScreenHeader = $("small.text-muted");

    private void removeProductToCartList(String productId) {
        SelenideElement productSelected = this.getProductElement(productId);
        SelenideElement button = productSelected.parent().parent().$("[data-icon='cart-plus']");
        button.click();
    }

    private void addProductCartList(String productId) {
        SelenideElement product = this.getProductElement(productId);
        SelenideElement cartButton = product.parent().parent().$("[data-icon='cart-plus']");
        cartButton.click();
    }

    private SelenideElement getProductElement(String productId) {
        SelenideElement productSelected = $(String.format(".card-link[href='#/product/%s']", productId));
        return productSelected;
    }

    public SelenideElement getProductCartElement(String productId) {
        SelenideElement productSelected = $(String.format("[href='#product/%s']", productId));
        return productSelected;
    }

    private SelenideElement getCartProductDeleteElement(String productId) {
        SelenideElement product = this.getProductCartElement(productId);
        return product.parent().parent().$("[data-icon=trash]");
    }

    private SelenideElement getCartProductQtyMinusElement(String productId) {
        SelenideElement product = this.getProductCartElement(productId);
        return product.parent().parent().$("[data-icon=minus-circle]");
    }

    private String getProductPrice(String productId) {
        SelenideElement product = getProductElement((productId));
        return product.parent().parent().$(".card-footer p span").text();
    }

    private String getCartUnitPrice(String productId) {
        SelenideElement product = getProductCartElement((productId));
        return product.parent().parent().$("div:nth-child(2) > div").text();
    }

    private String getCartTotalPrice(String productId) {
        SelenideElement product = getProductCartElement((productId));
        return product.parent().parent().$("div:nth-child(3) > div").text();
    }


    private SelenideElement getCartProductQtyPlusElement(String productId) {
        SelenideElement product = this.getProductCartElement(productId);
        return product.parent().parent().$("[data-icon=plus-circle]");
    }

    private String getCartProductQty(String productId) {
        SelenideElement qtyElement = getCartProductQtyMinusElement(productId).parent().parent();
        return qtyElement.getText();
    }

    @Test
    //Test is product is added to cart list
    public void addToCartListTest() {
        Utils.openPage("https://fasttrackit-test.netlify.app/#/");
        this.addProductCartList("1");
        this.cartButton.click();

        SelenideElement product = getProductCartElement("1");
        sleep(2000);
        assertEquals(product.isDisplayed(), true);

    }

    @Test
    //Test if cart list badge shows correct number of products
    public void cardBadgeNumberTest() {
        Utils.openPage("https://fasttrackit-test.netlify.app/#/");
        this.addProductCartList("1");
        this.addProductCartList("2");

        assertEquals(shoppingCartBadge.getText(), "2");
    }

    @Test
    //Test deletion of a product from cart by pressing product button
    public void deleteProductFromCardTest() {
        Utils.openPage("https://fasttrackit-test.netlify.app/#/");
        this.addProductCartList("1");
        this.cartButton.click();

        SelenideElement cardProduct = getProductCartElement("1");
        assertEquals(cardProduct.isDisplayed(), true);

        SelenideElement deleteChartProduct = getCartProductDeleteElement("1");
        deleteChartProduct.click();

        assertEquals(cardProduct.isDisplayed(), false);
    }

    @Test
    //Test deletion of a product from cart by setting qty to zero
    public void deleteProduct2FromCardTest() {
        Utils.openPage("https://fasttrackit-test.netlify.app/#/");
        this.addProductCartList("1");
        this.cartButton.click();

        SelenideElement cardProduct = getProductCartElement("1");
        assertEquals(cardProduct.isDisplayed(), true);

        SelenideElement minusButton = getCartProductQtyMinusElement("1");
        minusButton.click();

        assertEquals(cardProduct.isDisplayed(), false);
    }

    @Test
    //Test pressing to chart multiple times
    public void clickChartMultipleTimesTest() {
        Utils.openPage("https://fasttrackit-test.netlify.app/#/");
        this.addProductCartList("1");
        this.addProductCartList("1");
        this.cartButton.click();

        String productNumber = this.getCartProductQty("1");

        assertEquals(productNumber, "2");
    }

    @Test
    //Test increase quanty in the product in chart
    public void increaseProductChartQtyTest() {
        Utils.openPage("https://fasttrackit-test.netlify.app/#/");
        this.addProductCartList("1");
        this.cartButton.click();

        SelenideElement plusButton = getCartProductQtyPlusElement("1");
        plusButton.click();

        String productNumber = this.getCartProductQty("1");

        assertEquals(productNumber, "2");
    }

    @Test
    //Test continue shopping will get back to home button
    public void continueShoopingTest() {
        Utils.openPage("https://fasttrackit-test.netlify.app/#/");
        this.addProductCartList("1");
        this.cartButton.click();

        continueShopping.click();

        assertEquals(homeScreenHeader.text(), "Products");
        assertEquals(shoppingCartBadge.text(), "1");
    }

    @Test
    //Test cost of product is same on cart and home screen
    public void productCostTest() {
        Utils.openPage("https://fasttrackit-test.netlify.app/#/");

        String productPrice = this.getProductPrice("1");
        this.addProductCartList("1");
        this.cartButton.click();

        String cartUnitPrice = this.getCartUnitPrice("1");

        assertEquals(productPrice, cartUnitPrice.replace("$", ""));

    }
}