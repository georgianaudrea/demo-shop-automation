package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class Utils {
    private static final SelenideElement resetButton= $("[data-icon=undo]");
    static void openPage(String page) {
        open(page);
        sleep(1000);
        resetProject();
    }
    static void resetProject()  {
        resetButton.click();
    }

}
