package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.testng.AssertJUnit.assertEquals;

public class LoginTest extends DemoShopConfiguration {

    private final SelenideElement loginButton =$("[data-icon=sign-in-alt]");
    private final SelenideElement userName = $("[data-test = username]");
    private final SelenideElement password= $( "[data-test = password]");
    private final SelenideElement modalLoginButton= $( "[data-test = password]~button");
    private final SelenideElement modalLogOutButton= $ ("[data-icon=sign-out-alt]");
    private final SelenideElement accountLink= $( "[href='#/account']");
    private final SelenideElement errorMessage= $ ( "[data-test=error]");


    private void openLoginModal() {
        Utils.openPage("https://fasttrackit-test.netlify.app/#/");
        loginButton.click();
    }

    private void loginUser(String user,String passwordText) {
        this.openLoginModal();
        userName.setValue(user);
        password.setValue(passwordText);
        modalLoginButton.click();
    }


    // login modal is displayed
    @Test
    public void loginModalTest () {

        this.openLoginModal();

        assertEquals(userName.isDisplayed(),true);
        assertEquals(password.isDisplayed(),true);
        assertEquals(modalLoginButton.isDisplayed(),true);
    }
    // login with correct username and password
    @Test
    public void loginSuccessTest () {

        this.loginUser("dino","choochoo");
        assertEquals(accountLink.isDisplayed(),true);
        assertEquals(accountLink.text(),"dino");
        modalLogOutButton.click();
    }
    // logOut button
    @Test
    public void logoutTest() {
        this.loginUser("dino","choochoo");
        modalLogOutButton.click();
        sleep(1000);
        assertEquals(loginButton.isDisplayed(), true);
        // assertEquals(modalLogOutButton.text(), "Hello Guest!");
    }
    // Incorrect username or password;
    @Test
    public void wrongCredentialsTest (){
        this.loginUser("dino1","choochoo1");
        assertEquals(errorMessage.isDisplayed(),true);
        assertEquals(errorMessage.text(),"Incorrect username or password!");
    }

}