package org.fasttrackit;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

public class DemoShopConfiguration {
    private static final String DEFAULT_BROWSER = "chrome";
    @BeforeClass
    static void allureReports() {
        SelenideLogger.addListener("allureSelenide", new AllureSelenide());
    }

    public DemoShopConfiguration(){
        Configuration.browser = DEFAULT_BROWSER;
        String browser = readValuesFromEnv();
        if (!browser.isBlank())
            Configuration.browser=browser;
    }
    private String readValuesFromEnv(){
        //reads input from us and returns it -> firefox
        return "";
    }

}
