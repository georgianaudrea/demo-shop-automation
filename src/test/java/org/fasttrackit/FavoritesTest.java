package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.$;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class FavoritesTest extends DemoShopConfiguration {

    private SelenideElement badgeCounter= $(".shopping_cart_badge");
    private SelenideElement wishlistTitle = $(".subheader-container h1 small");
    private SelenideElement wishListButton=$("[href='#/wishlist']");
    private SelenideElement cartButton = $("[href='#/cart']");

    private CartTest cartTest = new CartTest();

    private void openPage() {
        Utils.openPage("https://fasttrackit-test.netlify.app/#/");
    }

    private SelenideElement getProductSelector(String productId) {
        SelenideElement productSelected = $(String.format(".card-link[href='#/product/%s']", productId));
        return productSelected;
    }

    private void removeProductFromWishList(String productId) {
        SelenideElement productSelected = this.getProductSelector(productId);
        SelenideElement button = productSelected.parent().parent().$("button [data-icon=heart-broken]");
        button.click();
    }

    private void addProductToWishList(String productId) {
        SelenideElement productSelected = this.getProductSelector(productId);
        SelenideElement wishlistButton = productSelected.parent().parent().$("button [data-icon=heart]");
        wishlistButton.click();
    }
    private void addProductCartList(String productId) {
        SelenideElement product = this.getProductSelector(productId);
        SelenideElement cartButton= product.parent().parent().$("[data-icon='cart-plus']");
        cartButton.click();
    }
    @Test
    //add product to wish list
    public void addToWishList(){

         this.openPage();
         this.addProductToWishList("1");
         this.wishListButton.click();

         assertTrue(wishlistTitle.isDisplayed());
         assertEquals(wishlistTitle.getText(),"Wishlist");
         assertTrue(getProductSelector("1").isDisplayed());
    }

    @Test
    //add products to wishlist Counter
    public void badgeCounterTest(){
        this.openPage();
        this.addProductToWishList("1");
        this.addProductToWishList("2");

        assertEquals(badgeCounter.getText(),"2");
    }

    @Test
    //take product from wish list and brokenHeart Disapears
    public void removeProductFromWishListTest() {
        this.openPage();
        this.addProductToWishList("1");
        assertEquals(badgeCounter.getText(),"1");

        this.removeProductFromWishList("1");
        assertEquals(badgeCounter.isDisplayed(),false);
    }

    @Test
    //take product from wish list and brokenHeart Disapears
    public void removeProductFromWishlistPageTest() {
        this.openPage();
        this.addProductToWishList("1");
        assertEquals(badgeCounter.getText(),"1");
        wishListButton.click();

        this.removeProductFromWishList("1");
        assertEquals(badgeCounter.isDisplayed(),false);
    }
    @Test
    //add product from wish list to cart list
    public void movePoductFromWishListToCartList() {
        this.openPage();
        this.addProductToWishList("1");
        wishListButton.click();
        this.addProductCartList("1");
        this.cartButton.click();
        assertTrue(cartTest.getProductCartElement("1").isDisplayed());


    }
}
