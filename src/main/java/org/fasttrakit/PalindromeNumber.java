package org.fasttrakit;

public class PalindromeNumber {
  public static void calculate (){

      int num = 5336, reversedNumber = 0, reminder;
      //store de number to originalNum
      int originalNum = num;
      while (num != 0 ) {
          reminder = num % 10;
          reversedNumber = reversedNumber * 10 + reminder;
          num /= 10;
      }
           //check if reversedNum and originalNum are equal
      if ( originalNum == reversedNumber) {
          System.out.println(originalNum + "is Palindrome.");
      }
      else {
          System.out.println( originalNum + "is not Palindrome.");
      }
  }
}
